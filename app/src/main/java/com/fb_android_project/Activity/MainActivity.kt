package com.fb_android_project.Activity

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.LinearLayout
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.fb_android_project.Adapter.Tab_Bar_Adapter
import com.fb_android_project.Api.ApiCall
import com.fb_android_project.Base.BaseActivity
import com.fb_android_project.Pojo_Model.FBApi
import com.fb_android_project.R
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.botom_menu_dialog.*
import kotlinx.android.synthetic.main.no_network_layout.*
import kotlinx.android.synthetic.main.progress_loading_layout.*
import kotlinx.android.synthetic.main.something_wrong_layout.*
import retrofit2.Call
import retrofit2.Response
import java.util.ArrayList
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.widget.TextView
import com.fb_android_project.Adapter.Cart_Adapter
import com.fb_android_project.Array_Model.Cart_item_Arraymodel
import com.fb_android_project.Fragment.Food_Category_Fragment
import com.fb_android_project.Pojo_Model.FoodList
import kotlinx.android.synthetic.main.tab_viewpager.*


class MainActivity : BaseActivity() {
    internal lateinit var gson: Gson
    private val tab_array = ArrayList<String>()
    lateinit var tabadapter: Tab_Bar_Adapter
    var foodlistpojo: List<FoodList>? = null
    companion object {
        internal var cartarray = ArrayList<Cart_item_Arraymodel>()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tab_recylerview.setHasFixedSize(true)
        val llm1 = LinearLayoutManager(applicationContext)
        llm1.orientation = LinearLayoutManager.HORIZONTAL
        tab_recylerview.layoutManager = llm1
        tabadapter = Tab_Bar_Adapter(
            applicationContext,
            tab_array,
            object : Tab_Bar_Adapter.Tab_Bar_Adapterclick {
                override fun tabupdate(position: Int) {
                    viewPager!!.setCurrentItem(position, true);
                }
            })
        tab_recylerview.adapter = tabadapter
        payamountlayout.setOnClickListener {
            if (bottom_layout.visibility == View.VISIBLE) {
                layout_animation(bottom_layout, R.anim.bottom_anim)
                bottom_layout.visibility = View.GONE
            } else if (bottom_layout.visibility == View.GONE) {
                bottom_layout.visibility = View.VISIBLE
                layout_animation(bottom_layout, R.anim.top_anim)
            }
        }
        call_api()
    }


    private fun load_foodlist() {
        gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .create()
        val apiService = ApiCall.create()
        val call = apiService.Food_List()

        call.enqueue(object : retrofit2.Callback<FBApi> {
            override fun onFailure(call: Call<FBApi>?, t: Throwable?) {
                progress_loading.visibility = View.GONE
                somethingwrongfun()
                System.out.println("Errorcheck" + t!!.message)
            }

            override fun onResponse(call: Call<FBApi>?, response: Response<FBApi>?) {
                progress_loading.visibility = View.GONE
                if (response!!.body() != null) {
                    if (response.code() == 200) {
                        System.out.println("ResponceCheck" + Gson().toJson(response))
                        val adapter = ViewPagerAdapter(supportFragmentManager)
                        foodlistpojo = response.body().getFoodList()!!
                        System.out.println("checksize" + response.body().getFoodList()!!.size)
                        for (i in 0 until response.body().getFoodList()!!.size) {
                            tab_array.add(response.body().getFoodList()!!.get(i).getTabName().toString())
                            adapter.addFrag(
                                Food_Category_Fragment(
                                    foodlistpojo,
                                    i,
                                    response.body().getCurrency()
                                )
                            )
                            viewPager.adapter = adapter
                        }

                        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                            override fun onPageScrolled(i: Int, v: Float, i1: Int) {

                            }

                            override fun onPageSelected(position: Int) {
                                System.out.println("checkresfrshh")
                                //     adapter.notifyDataSetChanged()
                            }

                            override fun onPageScrollStateChanged(i: Int) {

                            }
                        })
                        tabadapter.notifyDataSetChanged()
                    } else {
                        somethingwrongfun()
                    }
                } else {
                    somethingwrongfun()
                }
            }
        })


    }

    fun call_api() {
        if (isNetworkAvailable) {
            progress_loading.visibility = View.VISIBLE
            load_foodlist()
        } else {
            no_internet()
        }
    }

    fun somethingwrongfun() {
        somethingwrong_layout.visibility = View.VISIBLE
        somethingwrong_retry.setOnClickListener {
            somethingwrong_layout.visibility = View.GONE
            call_api()
        }

    }

    fun no_internet() {
        no_network_layout.visibility = View.VISIBLE
        nonetwork_retry.setOnClickListener {
            progress_loading.visibility = View.VISIBLE
            no_network_layout.visibility = View.GONE
            call_api()
        }

    }


    fun layout_animation(layout: LinearLayout, animation_folder: Int) {
        val animation: Animation
        animation = AnimationUtils.loadAnimation(
            applicationContext,
            animation_folder
        )
        layout.setAnimation(animation)
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) :
        FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFrag(fragment: Fragment) {
            mFragmentList.add(fragment)
        }
    }


}
