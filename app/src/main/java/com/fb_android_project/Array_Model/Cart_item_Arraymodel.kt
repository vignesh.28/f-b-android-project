package com.fb_android_project.Array_Model

import android.R.attr.name



class Cart_item_Arraymodel {
    private var Proname: String? = null
    private var proid: String? = null
    private var price: Double? = null
    private var currency:String?=null
    private var count: Int? = null
    private var total: Double? = null

    constructor(Proname: String,proid: String,price: Double,currency: String,count:Int,total:Double) {
        this.Proname = Proname
        this.proid = proid
        this.price = price
        this.currency = currency
        this.count = count
        this.total = total
    }


    fun getProname(): String? {
        return Proname
    }

    fun setProname(Proname: String) {
        this.Proname = Proname
    }
    fun getproid(): String? {
        return proid
    }

    fun setproid(proid: String) {
        this.proid = proid
    }
    fun getprice(): Double? {
        return price
    }

    fun setprice(price: Double) {
        this.price = price
    }
    fun getcurrency(): String? {
        return currency
    }

    fun setcurrency(currency: String) {
        this.currency = currency
    }

    fun getcount(): Int? {
        return count
    }

    fun setcount(count: Int) {
        this.count = count
    }

    fun gettotal(): Double? {
        return total
    }

    fun settotal(total: Double) {
        this.total = total
    }

}
