package com.fb_android_project.Fragment


import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import com.fb_android_project.Activity.MainActivity
import com.fb_android_project.Adapter.Cart_Adapter
import com.fb_android_project.Adapter.Foodlist_Adapter
import com.fb_android_project.Array_Model.Cart_item_Arraymodel
import com.fb_android_project.Base.BaseFragment
import com.fb_android_project.Pojo_Model.Fnblist
import com.fb_android_project.Pojo_Model.FoodList
import com.fb_android_project.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.botom_menu_dialog.*
import java.util.ArrayList
import android.text.method.TextKeyListener.clear
import android.R.attr.name
import android.location.Geocoder.isPresent
import com.fb_android_project.Activity.MainActivity.Companion.cartarray
import java.util.function.Predicate






@SuppressLint("ValidFragment")
class Food_Category_Fragment(
    foodlistpojo: List<FoodList>?,
    position: Int,
    currency: String?
) : BaseFragment() {
    var foodlistpojo: List<FoodList>? = foodlistpojo
    var position: Int = position
    var currency: String = currency.toString()
    lateinit var foodadapter: Foodlist_Adapter
    var foodlistarray: List<Fnblist>? = null
    lateinit var foodlist_recylerview: RecyclerView
    lateinit var bottom_cart_recylerview: RecyclerView
    lateinit var cartadapter: Cart_Adapter
    var total = 0

    lateinit var totalamount:TextView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.food_list_layout, container, false)
        foodlist_recylerview = view.findViewById(R.id.foodlist_recylerview)
        foodlist_recylerview.setHasFixedSize(true)
        val llm1 = LinearLayoutManager(activity)
        llm1.orientation = LinearLayoutManager.VERTICAL
        foodlist_recylerview.layoutManager = llm1
        totalamount=activity!!.findViewById(R.id.totalamount)
        bottom_cart_recylerview = activity!!.findViewById(R.id.bottom_cart_recylerview)
        bottom_cart_recylerview.setHasFixedSize(true)
        val llm = LinearLayoutManager(activity)
        llm.orientation = LinearLayoutManager.VERTICAL
        bottom_cart_recylerview.layoutManager = llm
        totalamount.setText(currency+" 0.0")


        foodlistarray = foodlistpojo!!.get(position).getFnblist()
        foodadapter = Foodlist_Adapter(
            activity,
            foodlistarray,
            currency,
            object : Foodlist_Adapter.Foodlist_Adapterclick {
                override fun removevalues(
                    position: Int,
                    proid: String,
                    count: Int,
                    price: String,
                    proname: String
                ) {
                    cartvalueupdate(proid, count, price.toDouble(), currency, proname)
                }

                override fun addvalues(
                    position: Int,
                    proid: String,
                    count: Int,
                    price: String,
                    proname: String
                ) {
                    cartvalueupdate(proid, count, price.toDouble(), currency, proname)
                }

            })
        foodlist_recylerview.adapter = foodadapter
        System.out.println("checkcoome" + position)
        System.out.println("checkfirstgson" + Gson().toJson(foodlistpojo))
        animlayout(foodlist_recylerview)
        return view
    }

    fun animlayout(layout: RecyclerView) {
        val anim = AnimationUtils.loadLayoutAnimation(activity, R.anim.anim_controller)
        layout.setLayoutAnimation(anim)
    }

    fun cartvalueupdate(
        proid: String,
        count: Int,
        price: Double,
        currency: String,
        proname: String
    ) {

        if (!containsLocation(cartarray,proid,count,count*price)){
            cartarray.add(
                Cart_item_Arraymodel(
                    proname,
                    proid,
                    price,
                    currency,
                    count,
                    count * price
                )
            )
        }
       /* for (i in 0 until cartarray.size) {
            if (cartarray.get(i).getcount()==0){
                cartarray.removeAt(i)
            }
        }*/
        cartadapter = Cart_Adapter(
            activity, cartarray, object : Cart_Adapter.Cart_Adapterclick {
                override fun tabupdate(position: Int) {
                }
            })
        bottom_cart_recylerview.adapter = cartadapter


        totalamount.setText(currency+" "+grandTotal().toString())
        System.out.println("grandTotal"+grandTotal())
    }

    fun containsLocation(c: Collection<Cart_item_Arraymodel>, proid: String,count: Int,price: Double): Boolean {
        for (o in c) {
            System.out.println("checko"+proid+"   "+o.getproid())
            if (o != null && o!!.getproid()!!.contains(proid)) {
                o.setcount(count)
                o.settotal(price)
                return true
            }
        }
        return false
    }


    private fun grandTotal(): Double {
        var totalPrice = 0.0
        for (i in 0 until cartarray!!.size) {
            totalPrice += cartarray[i].gettotal()!!
        }
        return totalPrice
    }
}