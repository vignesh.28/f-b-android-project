package com.fb_android_project.Pojo_Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Status {
    @SerializedName("Id")
    @Expose
    private var id: String? = null
    @SerializedName("Description")
    @Expose
    private var description: String? = null

    fun getId(): String? {
        return id
    }

    fun setId(id: String) {
        this.id = id
    }

    fun getDescription(): String? {
        return description
    }

    fun setDescription(description: String) {
        this.description = description
    }
}