package com.fb_android_project.Pojo_Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Subitem {
    @SerializedName("Name")
    @Expose
    private var name: String? = null
    @SerializedName("PriceInCents")
    @Expose
    private var priceInCents: String? = null
    @SerializedName("SubitemPrice")
    @Expose
    private var subitemPrice: String? = null
    @SerializedName("VistaParentFoodItemId")
    @Expose
    private var vistaParentFoodItemId: String? = null
    @SerializedName("VistaSubFoodItemId")
    @Expose
    private var vistaSubFoodItemId: String? = null

    fun getName(): String? {
        return name
    }

    fun setName(name: String) {
        this.name = name
    }

    fun getPriceInCents(): String? {
        return priceInCents
    }

    fun setPriceInCents(priceInCents: String) {
        this.priceInCents = priceInCents
    }

    fun getSubitemPrice(): String? {
        return subitemPrice
    }

    fun setSubitemPrice(subitemPrice: String) {
        this.subitemPrice = subitemPrice
    }

    fun getVistaParentFoodItemId(): String? {
        return vistaParentFoodItemId
    }

    fun setVistaParentFoodItemId(vistaParentFoodItemId: String) {
        this.vistaParentFoodItemId = vistaParentFoodItemId
    }

    fun getVistaSubFoodItemId(): String? {
        return vistaSubFoodItemId
    }

    fun setVistaSubFoodItemId(vistaSubFoodItemId: String) {
        this.vistaSubFoodItemId = vistaSubFoodItemId
    }


}