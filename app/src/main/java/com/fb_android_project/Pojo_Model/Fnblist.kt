package com.fb_android_project.Pojo_Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Fnblist {

    @SerializedName("Cinemaid")
    @Expose
    private var cinemaid: String? = null
    @SerializedName("TabName")
    @Expose
    private var tabName: String? = null
    @SerializedName("VistaFoodItemId")
    @Expose
    private var vistaFoodItemId: String? = null
    @SerializedName("Name")
    @Expose
    private var name: String? = null
    @SerializedName("PriceInCents")
    @Expose
    private var priceInCents: String? = null
    @SerializedName("ItemPrice")
    @Expose
    private var itemPrice: String? = null
    @SerializedName("SevenStarExperience")
    @Expose
    private var sevenStarExperience: String? = null
    @SerializedName("OtherExperience")
    @Expose
    private var otherExperience: String? = null
    @SerializedName("SubItemCount")
    @Expose
    private var subItemCount: Int = 0
    @SerializedName("ImageUrl")
    @Expose
    private var imageUrl: String? = null
    @SerializedName("ImgUrl")
    @Expose
    private var imgUrl: String? = null
    @SerializedName("VegClass")
    @Expose
    private var vegClass: String? = null
    @SerializedName("subitems")
    @Expose
    private var subitems: List<Subitem>? = null

    fun getCinemaid(): String? {
        return cinemaid
    }

    fun setCinemaid(cinemaid: String) {
        this.cinemaid = cinemaid
    }

    fun getTabName(): String? {
        return tabName
    }

    fun setTabName(tabName: String) {
        this.tabName = tabName
    }

    fun getVistaFoodItemId(): String? {
        return vistaFoodItemId
    }

    fun setVistaFoodItemId(vistaFoodItemId: String) {
        this.vistaFoodItemId = vistaFoodItemId
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String) {
        this.name = name
    }

    fun getPriceInCents(): String? {
        return priceInCents
    }

    fun setPriceInCents(priceInCents: String) {
        this.priceInCents = priceInCents
    }

    fun getItemPrice(): String? {
        return itemPrice
    }

    fun setItemPrice(itemPrice: String) {
        this.itemPrice = itemPrice
    }

    fun getSevenStarExperience(): String? {
        return sevenStarExperience
    }

    fun setSevenStarExperience(sevenStarExperience: String) {
        this.sevenStarExperience = sevenStarExperience
    }

    fun getOtherExperience(): String? {
        return otherExperience
    }

    fun setOtherExperience(otherExperience: String) {
        this.otherExperience = otherExperience
    }

    fun getSubItemCount(): Int {
        return subItemCount
    }

    fun setSubItemCount(subItemCount: Int) {
        this.subItemCount = subItemCount
    }

    fun getImageUrl(): String? {
        return imageUrl
    }

    fun setImageUrl(imageUrl: String) {
        this.imageUrl = imageUrl
    }

    fun getImgUrl(): String? {
        return imgUrl
    }

    fun setImgUrl(imgUrl: String) {
        this.imgUrl = imgUrl
    }

    fun getVegClass(): String? {
        return vegClass
    }

    fun setVegClass(vegClass: String) {
        this.vegClass = vegClass
    }

    fun getSubitems(): List<Subitem>? {
        return subitems
    }

    fun setSubitems(subitems: List<Subitem>) {
        this.subitems = subitems
    }

}