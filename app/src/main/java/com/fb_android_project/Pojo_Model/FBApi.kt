package com.fb_android_project.Pojo_Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class FBApi{
    @SerializedName("status")
    @Expose
    private var status: Status? = null
    @SerializedName("Currency")
    @Expose
    private var currency: String? = null
    @SerializedName("FoodList")
    @Expose
    private var foodList: List<FoodList>? = null

    fun getStatus(): Status? {
        return status
    }

    fun setStatus(status: Status) {
        this.status = status
    }

    fun getCurrency(): String? {
        return currency
    }

    fun setCurrency(currency: String) {
        this.currency = currency
    }

    fun getFoodList(): List<FoodList>? {
        return foodList
    }

    fun setFoodList(foodList: List<FoodList>) {
        this.foodList = foodList
    }

}