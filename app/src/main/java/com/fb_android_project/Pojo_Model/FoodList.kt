package com.fb_android_project.Pojo_Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class FoodList {
    @SerializedName("TabName")
    @Expose
    private var tabName: String? = null
    @SerializedName("fnblist")
    @Expose
    private var fnblist: List<Fnblist>? = null

    fun getTabName(): String? {
        return tabName
    }

    fun setTabName(tabName: String) {
        this.tabName = tabName
    }

    fun getFnblist(): List<Fnblist>? {
        return fnblist
    }

    fun setFnblist(fnblist: List<Fnblist>) {
        this.fnblist = fnblist
    }
}