package com.fb_android_project.Adapter

import android.content.Context
import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.fb_android_project.R
import java.util.ArrayList


class Tab_Bar_Adapter(context: Context?, internal var tabarray: ArrayList<String>, var onClickListener: Tab_Bar_Adapter.Tab_Bar_Adapterclick) : RecyclerView.Adapter<Tab_Bar_Adapter.ViewHolder>(), View.OnClickListener {

    internal lateinit var context1: Context
    internal lateinit  var displaysize: String
    internal var pharmacynotif: String? = null
    var row_index:Int = 0

    init {
        var context = context
        if (context != null) {
            context1 = context
        }
    }


    interface Tab_Bar_Adapterclick {

        fun tabupdate(position: Int)
    }


    override fun onClick(view: View) {

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.tab_top_item, parent, false)
        return ViewHolder(view)
        //return null;
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.titlename.setText(tabarray.get(position))
        if (position==tabarray.size-1){
            holder.rightview.visibility=View.GONE
        }
        else{
            holder.rightview.visibility=View.VISIBLE
        }

        holder.tablayout.setOnClickListener {
            onClickListener.tabupdate(position)
            row_index=position
            notifyDataSetChanged()
        }
        if (row_index==position){
            holder.bottom_view.visibility=View.VISIBLE
            holder.titlename.setTypeface(null,Typeface.BOLD)
        }
        else{
            holder.bottom_view.visibility=View.GONE
            holder.titlename.setTypeface(null,Typeface.NORMAL)
        }
    }


    override fun getItemCount(): Int {
        return tabarray.size

    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        internal var  titlename: TextView
        internal var  bottom_view: View
        internal var  rightview: View
        lateinit var tablayout:LinearLayout

        init {
            context1 = itemView.context


            titlename = itemView.findViewById<View>(R.id.titlename) as TextView
            bottom_view = itemView.findViewById<View>(R.id.bottom_view) as View
            rightview = itemView.findViewById<View>(R.id.right_view) as View
            tablayout=itemView.findViewById(R.id.tab_layout) as LinearLayout
        }
    }

}