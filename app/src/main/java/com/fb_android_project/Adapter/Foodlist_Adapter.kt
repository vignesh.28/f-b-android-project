package com.fb_android_project.Adapter

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.fb_android_project.Pojo_Model.Fnblist
import com.fb_android_project.R
import com.squareup.picasso.Picasso


class Foodlist_Adapter(
    context: Context?,
    internal var fnblist: List<Fnblist>?,var currency:String,
    var onClickListener: Foodlist_Adapterclick
) : RecyclerView.Adapter<Foodlist_Adapter.ViewHolder>(), View.OnClickListener {

    internal lateinit var context1: Context
    internal lateinit var displaysize: String
    internal var pharmacynotif: String? = null
    var row_index: Int = 0
    var cartcount: Int = 0
    lateinit var subitem_adapter: Foodlist_Subitem_Adapter
    lateinit var pricevalue:String
    init {
        var context = context
        if (context != null) {
            context1 = context
        }
    }


    interface Foodlist_Adapterclick {

        fun addvalues(position: Int,proid:String,count:Int,price:String,proname:String)
        fun removevalues(position: Int,proid:String,count:Int,price:String,proname:String)

    }


    override fun onClick(view: View) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.foodlis_item, parent, false)
        return ViewHolder(view)
        //return null;
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.foodname.setText(fnblist!!.get(position).getName())

        if (!fnblist!!.get(position).getImageUrl().equals("")){
            Picasso.get()
                .load(fnblist!!.get(position).getImageUrl())
                .placeholder(R.drawable.food_placeholder)
                .into(holder.food_image)
        }
        else{
            holder.food_image!!.setBackgroundResource(R.drawable.food_placeholder)
        }
        holder.foodprice.setText(currency+" "+fnblist!!.get(position).getItemPrice())

        holder.cartcount.setText(cartcount.toString())
        holder.subitem_recylerview.setHasFixedSize(true)
        val llm1 = LinearLayoutManager(context1)
        llm1.orientation = LinearLayoutManager.HORIZONTAL
        holder.subitem_recylerview.layoutManager = llm1

        if (fnblist!!.size!=0){
            subitem_adapter= Foodlist_Subitem_Adapter(context1,fnblist!!.get(position).getSubitems(),object : Foodlist_Subitem_Adapter.Foodlist_Subitem_Adapterclick {
                override fun subitemdetails(ivalue: Int, subitemPrice: String?,click:Boolean) {
                    holder.foodprice.setText(currency+" "+subitemPrice)
                    holder.foodprice.tag=subitemPrice
                    pricevalue= subitemPrice.toString()
                    System.out.println("checkprice"+pricevalue)
                    if (click){
                        onClickListener.addvalues(
                            position,
                            fnblist!!.get(position).getVistaFoodItemId().toString(),
                            holder.cartcount.text.toString().toInt(),
                            holder.foodprice.tag.toString(),fnblist!!.get(position).getName().toString()
                        )
                    }

                }
            })
        }

            pricevalue= fnblist!!.get(position).getItemPrice().toString()
        holder.foodprice.tag=pricevalue


        if (fnblist!!.get(position).getSubitems()!!.size!=0){
            holder.subitem_recylerview.adapter=subitem_adapter
        }
        holder.addcart.setOnClickListener(View.OnClickListener { view ->
            cartcount = Integer.parseInt(holder.cartcount.text.toString())

            cartcount++
            holder.cartcount.setText(cartcount.toString())

            System.out.println("checkpricevalues"+pricevalue)

              onClickListener.addvalues(
                  position,
                  fnblist!!.get(position).getVistaFoodItemId().toString(),
                  holder.cartcount.text.toString().toInt(),
                  holder.foodprice.tag.toString(),fnblist!!.get(position).getName().toString()
              )
        })
        holder.removecart.setOnClickListener { view ->
            cartcount = Integer.parseInt(holder.cartcount.text.toString())
            if (cartcount!=0){
                cartcount--
                holder.cartcount.setText(cartcount.toString())
            }
            onClickListener.removevalues(
                position,
                fnblist!!.get(position).getVistaFoodItemId().toString(),
                holder.cartcount.text.toString().toInt(),
                holder.foodprice.tag.toString(), fnblist!!.get(position).getName().toString()
            )
        }

    }


    override fun getItemCount(): Int {
        return fnblist!!.size

    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        internal var foodname: TextView
        internal var foodprice: TextView
        internal var removecart: ImageView
        lateinit var cartcount: TextView
        lateinit var addcart: ImageView
        lateinit var food_image:ImageView
        lateinit var subitem_recylerview:RecyclerView

        init {
            context1 = itemView.context


            foodname = itemView.findViewById<View>(R.id.foodname) as TextView
            foodprice = itemView.findViewById<View>(R.id.foodprice) as TextView
            removecart = itemView.findViewById<View>(R.id.removecart) as ImageView
            cartcount = itemView.findViewById<View>(R.id.cartcount) as TextView
            addcart = itemView.findViewById<View>(R.id.addcart) as ImageView
            food_image=itemView.findViewById<View>(R.id.food_image) as ImageView
            subitem_recylerview=itemView.findViewById<View>(R.id.subitem_recylerview) as RecyclerView
        }
    }

}
