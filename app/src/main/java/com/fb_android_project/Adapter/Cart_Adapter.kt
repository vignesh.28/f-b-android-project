package com.fb_android_project.Adapter

import android.content.Context
import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.fb_android_project.Array_Model.Cart_item_Arraymodel
import com.fb_android_project.R
import java.util.ArrayList

class Cart_Adapter(context: Context?, internal var cartitemarray: ArrayList<Cart_item_Arraymodel>, var onClickListener: Cart_Adapter.Cart_Adapterclick) : RecyclerView.Adapter<Cart_Adapter.ViewHolder>(), View.OnClickListener {

    internal lateinit var context1: Context
    internal lateinit  var displaysize: String
    internal var pharmacynotif: String? = null
    var row_index:Int = 0

    init {
        var context = context
        if (context != null) {
            context1 = context
        }
    }


    interface Cart_Adapterclick {

        fun tabupdate(position: Int)
    }


    override fun onClick(view: View) {

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cart_item, parent, false)
        return ViewHolder(view)
        //return null;
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemdetails.setText(cartitemarray.get(position).getProname()+" ("+cartitemarray.get(position).getcount().toString()+")")
        holder.itemprice.setText(cartitemarray.get(position).gettotal().toString())
        System.out.println("checkadapter"+position)
    }


    override fun getItemCount(): Int {
        return cartitemarray.size

    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        internal var  itemdetails: TextView
        internal var  itemprice: TextView

        init {
            context1 = itemView.context
            itemdetails = itemView.findViewById<View>(R.id.itemdetails) as TextView
            itemprice = itemView.findViewById<View>(R.id.itemprice) as TextView

        }
    }

}