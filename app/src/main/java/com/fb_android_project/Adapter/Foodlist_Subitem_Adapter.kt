package com.fb_android_project.Adapter

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.fb_android_project.Pojo_Model.Subitem
import com.fb_android_project.R


class Foodlist_Subitem_Adapter(context: Context?, internal var subitem: List<Subitem>?, var onClickListener: Foodlist_Subitem_Adapter.Foodlist_Subitem_Adapterclick) : RecyclerView.Adapter<Foodlist_Subitem_Adapter.ViewHolder>(), View.OnClickListener {

    internal lateinit var context1: Context
    internal lateinit  var displaysize: String
    internal var pharmacynotif: String? = null
    var row_index:Int = 0

    init {
        var context = context
        if (context != null) {
            context1 = context
        }
    }


    interface Foodlist_Subitem_Adapterclick {

        fun subitemdetails(position: Int, subitemPrice: String?,click:Boolean)
    }


    override fun onClick(view: View) {

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.sub_item_layout, parent, false)
        return ViewHolder(view)
        //return null;
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.subitemname.setText(subitem!!.get(position).getName())

        holder.subitem_layout.setOnClickListener {
            onClickListener.subitemdetails(position,subitem!!.get(position).getSubitemPrice(),true)
            row_index=position
            notifyDataSetChanged()
        }

        if (row_index==position){
            holder.subitemname.setTextColor(Color.parseColor("#000000"))
            holder.subitem_layout.setBackgroundResource(R.drawable.yellow_drawable)
            onClickListener.subitemdetails(position,subitem!!.get(position).getSubitemPrice(),false)
        }
        else{
            holder.subitemname.setTextColor(Color.parseColor("#B3B1B1"))
            holder.subitem_layout.setBackgroundResource(R.drawable.gray_border_drawable)

        }
    }


    override fun getItemCount(): Int {
        return subitem!!.size

    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        internal var  subitemname: TextView
        lateinit var subitem_layout: LinearLayout

        init {
            context1 = itemView.context


            subitemname = itemView.findViewById<View>(R.id.subitemname) as TextView
            subitem_layout=itemView.findViewById(R.id.subitem_layout) as LinearLayout
        }
    }

}