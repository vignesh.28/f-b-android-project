package com.fb_android_project.Base

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.preference.PreferenceManager
import android.widget.TextView
import android.widget.Toast
import com.fb_android_project.Activity.MainActivity
import com.fb_android_project.R


/**
 * Created by systimanx on 27/4/18.
 */

open class BaseFragment : android.support.v4.app.Fragment() {
    internal lateinit var manager: ConnectivityManager

    val isNetworkAvailable: Boolean
        get() {
            if (activity != null) {
                manager = activity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            }
            val info = manager.activeNetworkInfo
            return info != null && info.isConnected

        }


    fun showFragment(fragment: android.support.v4.app.Fragment?, title: String) {
        if (fragment != null) {
            val fragmentManager = (activity as MainActivity).getSupportFragmentManager()
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.flContent, fragment)
            fragmentTransaction.commit()
            // set the toolbar title
           // (activity as kot).getSupportActionBar().setTitle(title)
        }
    }
    private fun initializeSharedPreference() {

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity)
    }

    fun updateSharedPreference(key: String, value: String) {
        if (sharedPreferences == null) {
            initializeSharedPreference()
        }

        val editor = sharedPreferences!!.edit()
        editor.putString(key, value)
        editor.commit()
    }

    fun clearPreferece() {
        if (sharedPreferences == null) {
            initializeSharedPreference()
        }
        sharedPreferences!!.edit().clear().commit()
    }

    fun updateSharedPreference(key: String, value: Boolean) {
        if (sharedPreferences == null) {
            initializeSharedPreference()
        }

        val editor = sharedPreferences!!.edit()
        editor.putString(key, java.lang.Boolean.toString(value))
        editor.commit()
    }

    fun getSharedPreferenceValue(key: String?): String? {
        if (sharedPreferences == null) {
            initializeSharedPreference()
        }

        return if (key != null) {
            sharedPreferences!!.getString(key, null)
        } else {
            null
        }
    }

    companion object {
        var sharedPreferences: SharedPreferences? = null
    }


}

