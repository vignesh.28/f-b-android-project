package com.fb_android_project.Base

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import android.widget.Toast
import com.fb_android_project.R
import java.util.concurrent.atomic.AtomicReference

/**
 * Created by systimanx on 2/5/18.
 */

open class BaseActivity : AppCompatActivity() {


    val isNetworkAvailable: Boolean
        get() {
            val manager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val info = manager.activeNetworkInfo
            return info != null && info.isConnected
        }


    fun showFragment(fragment: Fragment?, title: String) {
        if (fragment != null) {
            val fragmentManager = AtomicReference(this.supportFragmentManager)
            val fragmentTransaction = fragmentManager.get().beginTransaction()
            fragmentTransaction.replace(R.id.flContent, fragment)
            fragmentTransaction.commit()
        }
    }

    fun showActivity(secondActivity: Class<*>) {
        val i = Intent(
            this,
            secondActivity
        )
        startActivity(i)
        finish()
    }

    private fun initializeSharedPreference() {

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
    }

    fun updateSharedPreference(key: String, value: String) {
        if (sharedPreferences == null) {
            initializeSharedPreference()
        }

        val editor = sharedPreferences!!.edit()
        editor.putString(key, value)
        editor.commit()
    }

    fun clearPreferece() {
        if (sharedPreferences == null) {
            initializeSharedPreference()
        }
        sharedPreferences!!.edit().clear().commit()
    }

    fun updateSharedPreference(key: String, value: Boolean) {
        if (sharedPreferences == null) {
            initializeSharedPreference()
        }

        val editor = sharedPreferences!!.edit()
        editor.putString(key, java.lang.Boolean.toString(value))
        editor.commit()
    }

    fun getSharedPreferenceValue(key: String?): String? {
        if (sharedPreferences == null) {
            initializeSharedPreference()
        }

        return if (key != null) {
            sharedPreferences!!.getString(key, null)
        } else {
            null
        }
    }

    companion object {

        var sharedPreferences: SharedPreferences? = null
    }

    fun Context.toast(message: CharSequence) =
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()


}