package com.fb_android_project.Base

import android.R
import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RelativeLayout


class ProgressBarHandler(private val mContext: Context) {
    private val mProgressBar: ProgressBar

    init {

        val layout = (mContext as Activity).findViewById<View>(android.R.id.content).rootView as ViewGroup

        mProgressBar = ProgressBar(mContext, null, R.attr.progressBarStyleLarge)
        //mProgressBar = new ProgressBar(context);
        // mProgressBar= new android.widget.ProgressBar(
        //    context,
        //    null,
        // android.R.attr.progressBarStyleLarge);
        mProgressBar.indeterminateDrawable.setColorFilter(Color.parseColor("#FFDC00"), android.graphics.PorterDuff.Mode.MULTIPLY)
        //mProgressBar.getProgressDrawable().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);
        mProgressBar.isIndeterminate = true
        /*  mProgressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(mContext, R.color.PrimaryColor),
                android.graphics.PorterDuff.Mode.MULTIPLY);
*/
        val params = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)

        val rl = RelativeLayout(mContext)

        rl.gravity = Gravity.CENTER
        rl.addView(mProgressBar)

        layout.addView(rl, params)

        hide()
    }

    fun show() {
        mProgressBar.visibility = View.VISIBLE
    }

    fun hide() {
        mProgressBar.visibility = View.INVISIBLE
    }
}
