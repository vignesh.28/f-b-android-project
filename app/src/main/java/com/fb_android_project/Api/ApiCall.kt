package com.fb_android_project.Api

import com.fb_android_project.Pojo_Model.FBApi
import com.google.gson.JsonObject
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*


interface ApiCall {

    @GET("5b700cff2e00005c009365cf")// use
    abstract fun Food_List(): Call<FBApi>

    companion object Factory {
        val BASE_URL = "http://www.mocky.io/v2/"

        fun create(): ApiCall {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create(ApiCall::class.java)
        }



    }
}
